package com.example.goldav.buybye;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.goldav.buybye.model.Item;
import com.example.goldav.buybye.model.ModelItem;
import com.example.goldav.buybye.model.Validation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddItemFragment extends Fragment {

    private static final String ARG_PARAM1 = "id";//if its edit screen so the id is not nnull
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String id;
    ImageView imageView;
    Bitmap imageBitmap;
    ProgressBar progressBar;


    private OnFragmentInteractionListener mListener;

    public AddItemFragment() {
        // Required empty public constructor
    }


    public static AddItemFragment newInstance(String param1) {
        AddItemFragment fragment = new AddItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
        setHasOptionsMenu(true);
    }
    public void onPrepareOptionsMenu(Menu menu)
    {
        menu.findItem(R.id.edititem).setVisible(false);
        menu.findItem(R.id.mainAdd).setVisible(false);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        menu.findItem(R.id.myItems).setVisible(true);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //import the edit texts
        View view = inflater.inflate(R.layout.fragment_add_item, container, false);
        final EditText name = (EditText) view.findViewById(R.id.ItemName);
        TextView titleText= (TextView) view.findViewById(R.id.titleText);
        final EditText price = (EditText) view.findViewById(R.id.ItemPrice);
        final EditText desc = (EditText) view.findViewById(R.id.itemDesc);
        final EditText location = (EditText) view.findViewById(R.id.itemLocation);
        final Item item ;
        Button delete= (Button) view.findViewById(R.id.deleteItem);
        progressBar = (ProgressBar) view.findViewById(R.id.mainProgressBar);
        if(id!=null) {//edit item case, so we to the set edit text values and also load the item image
            item = ModelItem.instace.getItem(id);
            name.setText(item.itemName);
            price.setText("" + item.price);
            titleText.setText("Edit item:");
            desc.setText(item.desc);
            location.setText(item.location);
            //if image is exsits so we load it into the image view
            if (item.imgUrl != null && !item.imgUrl.isEmpty() && !item.imgUrl.equals("")) {
                progressBar.setVisibility(View.VISIBLE);
                //image_lable.setVisibility(GONE);
                ModelItem.instace.getImage(item.imgUrl, new ModelItem.GetImageListener() {
                    @Override
                    public void onSuccess(Bitmap image) {
                        imageView.setImageBitmap(image);
                        progressBar.setVisibility(GONE);
                    }

                    @Override
                    public void onFail() {
                        MyAlert my = new MyAlert();
                        my.Message = "failed to load the image";
                        progressBar.setVisibility(GONE);

                        my.show(getFragmentManager(), "");
                    }
                });

            }
        }
        else {//add item case
            progressBar.setVisibility(GONE);
            delete.setVisibility(View.INVISIBLE);
            titleText.setText("add item:");
            item = new Item(true);
            item.imgUrl="";
            item.userId=MainActivity.mAuth.getCurrentUser().getUid();
        }
        imageView = (ImageView) view.findViewById(R.id.itemImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        //logical delete btn listener
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModelItem.instace.deleteItem(ModelItem.instace.getItem(id));
                mListener.finish();
            }
        });
        //cancel and pop back the last fragment
        Button cancel = (Button) view.findViewById(R.id.canceltem);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        // Inflate the layout for this fragment
        //save btn set a new / edit item
        Button save = (Button) view.findViewById(R.id.saveItem);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check if one of the edit texts is empty
                if(!Validation.CheckEditTextsEmpty(new EditText[]{desc,price,location,name})) {
                    try {
                        //check if the edit text of the price is a numeric
                        item.price = Double.valueOf(price.getText().toString());
                    }
                    catch (java.lang.NumberFormatException ex)//catch the error if the edit text of the price is not numeric
                    {
                        price.setError("price must to be a number");
                        return;
                    }
                    item.itemName = name.getText().toString();
                    item.location = location.getText().toString();
                    item.desc = desc.getText().toString();
                    item.isRemoved=0;
                }
                else
                    return;
                //save the image before we are adding the item into the fire base server
               progressBar.setVisibility(View.VISIBLE);
                if (imageBitmap != null) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                    ModelItem.instace.saveImage(imageBitmap, item.id +"AA"+(df.format(Calendar.getInstance().getTime()))+ ".jpeg",
                            new ModelItem.SaveImageListener() {
                        @Override
                        public void complete(String url) {//the image has been saved
                            item.imgUrl = url;
                            ModelItem.instace.addItem(item);//add or edit the item into fire base
                            progressBar.setVisibility(GONE);
                            mListener.finish();
                        }

                        @Override
                        public void fail() {
                            //notify operation fail,...
                            MyAlert my= new MyAlert();
                            my.Message="failed to save the image";
                            my.show(getFragmentManager(),"");
                            progressBar.setVisibility(GONE);
                        }
                    });
                }else{
                    ModelItem.instace.addItem(item);
                    progressBar.setVisibility(GONE);
                    mListener.finish();

                }
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
         public void finish();
    }
    //this function is called to camera intent
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    // the result of the camera intent
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }
}
