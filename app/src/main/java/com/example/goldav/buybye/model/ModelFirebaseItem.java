package com.example.goldav.buybye.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aviv Gold on 7/30/2017.
 */

public class ModelFirebaseItem {
    ChildEventListener listner = null;
    //add item to the fire base method
    public void addItem(Item item) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ItemSqlQuerys.ITEM_TABLE);
        Map<String, Object> value = new HashMap<>();
        value.put(ItemSqlQuerys.ITEM_ID, item.id);
        value.put(ItemSqlQuerys.ITEM_ITEMNAME, item.itemName);
        value.put(ItemSqlQuerys.ITEM_PRICE,item.price);
        value.put(ItemSqlQuerys.ITEM_DESC, item.desc);
        value.put(ItemSqlQuerys.ITEM_LOCATION, item.location);
        value.put(ItemSqlQuerys.ITEM_IMGURL, item.imgUrl);
        value.put(ItemSqlQuerys.ITEM_DELETE, item.isRemoved);
        value.put(ItemSqlQuerys.ITEM_USERID, item.userId);
        value.put(ItemSqlQuerys.ITEM_TIME, ServerValue.TIMESTAMP);
        myRef.child(item.id).setValue(value);
    }
    //log out from the fire method, remove the listener
    public void logout() {
        if(listner !=null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            database.getReference("items").removeEventListener(listner);
        }
        listner = null ;
    }
    interface RegisterItemUpdatesCallback{
        void onItemUpdate(Item item);
    }
    //gets updated from the server, on new/edit/delete items
    public void registerItemsUpdates(double lastUpdateDate,
                                        final RegisterItemUpdatesCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ItemSqlQuerys.ITEM_TABLE);
        //order the items by the last id, we want only items with have bigger last update date from us lastupdate data
        myRef.orderByChild(ItemSqlQuerys.ITEM_TIME).startAt(lastUpdateDate);
        listner= myRef.orderByChild("lastUpdateDate").startAt(lastUpdateDate)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Item item = dataSnapshot.getValue(Item.class);
                        callback.onItemUpdate(item);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Item item = dataSnapshot.getValue(Item.class);
                        callback.onItemUpdate(item);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Item item = dataSnapshot.getValue(Item.class);
                        callback.onItemUpdate(item);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Item item = dataSnapshot.getValue(Item.class);
                        callback.onItemUpdate(item);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
    //get image from the server
    public void getImage(String url, final ModelItem.GetImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference httpsReference = storage.getReferenceFromUrl(url);
        final long ONE_MEGABYTE = 1024 * 1024;
        httpsReference.getBytes(3* ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap image = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                listener.onSuccess(image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("TAG",exception.getMessage());
                listener.onFail();
            }
        });
    }
    //save image in the server
    public void saveImage(Bitmap imageBmp, String name, final ModelItem.SaveImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference imagesRef = storage.getReference().child("images").child(name);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                listener.complete(downloadUrl.toString());
            }
        });
    }

}
