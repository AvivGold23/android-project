package com.example.goldav.buybye.model;

/**
 * Created by Aviv Gold on 6/11/2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//creating the db file in the phone
public class ModelSqlItem extends SQLiteOpenHelper {
    ModelSqlItem(Context context) {
        super(context, "databaseitem.db", null, 13);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ItemSqlQuerys.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ItemSqlQuerys.onUpgrade(db, oldVersion, newVersion);
    }

}

