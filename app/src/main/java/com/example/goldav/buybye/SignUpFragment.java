package com.example.goldav.buybye;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.goldav.buybye.model.ModelUser;
import com.example.goldav.buybye.model.User;
import com.example.goldav.buybye.model.Validation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

//this fragment represented the sign up screen with the new user info
public class SignUpFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    User user = new User();//the new user of the app
    public SignUpFragment() {
    }


    public static SignUpFragment newInstance() {//factory method
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }
    //loading the view, and the new user data/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       final View v =inflater.inflate(R.layout.fragment_sign_up, container, false);
        final ProgressBar pb = (ProgressBar) v.findViewById(R.id.mainProgressBar5);

        Button send= (Button) v.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //import the edit text of the layout
                final EditText email = (EditText) v.findViewById(R.id.SignUpEmail);
                EditText pass = (EditText) v.findViewById(R.id.SignUpPass);
                final EditText firstName = (EditText) v.findViewById(R.id.SignUpFirstName);
                final EditText lastName = (EditText) v.findViewById(R.id.SignUpLastName);
                final EditText phone = (EditText) v.findViewById(R.id.SignUpPhone);
                //check if if of the editTexts is empty or null
                if(!Validation.CheckEditTextsEmpty(new EditText[]{email, firstName, pass, phone, lastName}))
                {
                        pb.setVisibility(View.VISIBLE);
                        MainActivity.mAuth.createUserWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            pb.setVisibility(View.INVISIBLE);

                                            //set new user with the current data
                                            user.setUser(firstName.getText().toString(), lastName.getText().toString(),
                                                    email.getText().toString(), phone.getText().toString());
                                            // Sign in success, update UI with the signed-in user's information
                                            MyAlert my = new MyAlert();
                                            my.Message = "success sign  up";
                                            my.show(getFragmentManager(), "");
                                            user.id=MainActivity.mAuth.getCurrentUser().getUid();//get user id from the fire base
                                            ModelUser.instace.addUser(user);//adding user into the fire base server
                                            mListener.SignUp();//sign into the app

                                        } else {
                                            pb.setVisibility(View.INVISIBLE);

                                            // If sign in fails, display a message to the user.
                                            MyAlert my = new MyAlert();
                                            my.Message = (task.getException().toString().split(":")[1]);
                                            my.show(getFragmentManager(), "");

                                        }

                                    }
                                });

                }



            }
        });

        
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnFragmentInteractionListener {
        void SignUp();
    }
}

