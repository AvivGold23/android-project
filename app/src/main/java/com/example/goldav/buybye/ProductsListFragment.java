package com.example.goldav.buybye;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.goldav.buybye.model.Item;
import com.example.goldav.buybye.model.ModelItem;
import com.example.goldav.buybye.model.ModelUser;
import com.example.goldav.buybye.model.User;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import static android.R.attr.data;

//this fragemnt is represented the list of the items with have number of options:
//* the first on the data member of id is  null so the fragment show all the items of the app
//*if the id is not null , so we show all specific user items
//* show search items
public class ProductsListFragment extends Fragment {

    ListView list;//list view of the fragment
    List<Item> data;//the data of the list
    ItemsListAdapter adapter;//the adpter of the list
    private static final String ARG_PARAM1 = "id";//id string in the hast map
    private String id;//id of the user
    private boolean search1 = false;
    private OnFragmentInteractionListener mListener;

    public ProductsListFragment() {
        // Required empty public constructor
    }

    //event bus function
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ModelItem.UpdateItemEvent event) {
        boolean exist = false;
        if ((!search1) && (id == null)) {
            for (int i = 0; i < data.size(); i++) {
                Item item = data.get(i);
                if (item.id.equals(event.item.id)) {//item is already exists in the data of the fragemnt
                    if (item.isRemoved == 1)
                        data.remove(i);
                    else {
                        //update case
                        data.set(i, event.item);
                    }
                    exist = true;
                    break;
                }

            }
            if ((!exist) && (event.item.isRemoved != 1)) {
                //add item case
                data.add(event.item);
            }
            adapter.notifyDataSetChanged();
            list.setSelection(adapter.getCount() - 1);
        }

    }


    public static ProductsListFragment newInstance(String param1) {
        ProductsListFragment fragment = new ProductsListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
        setHasOptionsMenu(true);

    }

    //setting the menu
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.edititem).setVisible(false);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
    }


    //creating the view of the fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            search1 = false;
            View view = inflater.inflate(R.layout.fragment_products_list, container, false);
            final TextView title = (TextView) view.findViewById(R.id.TitleList2);
            final Button search = (Button) view.findViewById(R.id.search);
            if (id == null) {
                data = ModelItem.instace.getAllItems();//all items of the app
                title.setText("BuyBye items:");
            } else {
                data = ModelItem.instace.userItems(id);//specifig user items
                ModelUser.instace.getUser(id, new ModelUser.GetUserCallback() {
                    @Override
                    public void onComplete(User user) {
                        title.setText(user.FirstName + " " + user.LastName + " items:");
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
            list = (ListView) view.findViewById(R.id.stlist_list);
            adapter = new ItemsListAdapter();
            list.setAdapter(adapter);

            final EditText searchText = (EditText) view.findViewById(R.id.searchText);
            // search listener
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    search1 = true;
                    String text = searchText.getText().toString();
                    if (text != null && !text.isEmpty() && !text.equals("")) {
                        data = ModelItem.instace.searchItems(text);//setting new data with have only the search items
                        adapter.notifyDataSetChanged();//update the adapter
                    }

                }
            });
            //click on the list
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    mListener.onFragmentInteraction(data.get(i).id);//load the details of the item
                }
            });
            return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onAttach(Activity context) {
        EventBus.getDefault().register(this);//register to the event bus

        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);//unregister from the event bus
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String id);
    }
    class ItemsListAdapter extends BaseAdapter {
        LayoutInflater inflater = getActivity().getLayoutInflater();////check

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }




        //creating the view of each item the list
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = inflater.inflate(R.layout.item_products_list, null);
            final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.strow_progressBar);
            final Item item = data.get(position);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView price = (TextView) convertView.findViewById(R.id.price);
            TextView name = (TextView) convertView.findViewById(R.id.nameItem22);
            name.setText(item.itemName);
            final ImageView imageView = (ImageView) convertView.findViewById(R.id.itemPhoto);
            title.setText(item.desc);
            price.setText(item.price+"");
            imageView.setTag(item.imgUrl);
            if (item.imgUrl != null && !item.imgUrl.isEmpty() && !item.imgUrl.equals("")){//load the item image
                progressBar.setVisibility(View.VISIBLE);
                ModelItem.instace.getImage(item.imgUrl, new ModelItem.GetImageListener() {
                    @Override
                    public void onSuccess(Bitmap image) {
                        String tagUrl = imageView.getTag().toString();
                        if (tagUrl.equals(item.imgUrl)) {
                            imageView.setImageBitmap(image);
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFail() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
            else
            {
                progressBar.setVisibility(View.GONE);

            }




            return convertView;
        }
    }
}
