package com.example.goldav.buybye.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aviv Gold on 7/30/2017.
 */

public class ItemSqlQuerys {
    //the rows and the table name defines
    public static final String ITEM_TABLE = "items";
    public static final String ITEM_USERID="userId";
   public  static final String ITEM_ID="id";
     public static final String ITEM_ITEMNAME="itemName";
    public static final String ITEM_PRICE="price";
    public static final String ITEM_DESC="desc";
    public static final String ITEM_LOCATION="location";
    public static final String ITEM_TIME="lastUpdateDate";
    public static final String ITEM_IMGURL="imgUrl";
    public static final String ITEM_DELETE="isRemoved";
    //get all items query
    public static List<Item> getAllItems(SQLiteDatabase db) {
        Cursor cursor = db.query(ITEM_TABLE, null, null, null, null, null, null);
        Log.d("check","get all query");
        List<Item> list = new LinkedList<Item>();
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(ITEM_ID);//
            int userIdIndex = cursor.getColumnIndex(ITEM_USERID);//
            int name = cursor.getColumnIndex(ITEM_ITEMNAME);
            int price = cursor.getColumnIndex(ITEM_PRICE);
            int desc =cursor.getColumnIndex(ITEM_DESC);
            int location =cursor.getColumnIndex(ITEM_LOCATION);
            int time =cursor.getColumnIndex(ITEM_TIME);
            int imgurl =cursor.getColumnIndex(ITEM_IMGURL);
            int delete =cursor.getColumnIndex(ITEM_DELETE);

            do {
                Item item = new Item(false);
                item.id = cursor.getString(idIndex);
                item.userId = cursor.getString(userIdIndex);
                item.itemName = cursor.getString(name);
                item.price = cursor.getFloat(price);
                item.desc=cursor.getString(desc);
                item.location = cursor.getString(location);
                item.lastUpdateDate = cursor.getDouble(time);
                item.imgUrl = cursor.getString(imgurl);
                item.isRemoved = cursor.getInt(delete);
                Log.d("itemdel",""+item.isRemoved);
                // we dont want to the add item with isremoved is 1.
                if(item.isRemoved !=1) {
                    list.add(item);
                }


            } while (cursor.moveToNext());
        }
        if(list.isEmpty())
            Log.d("check","get query");
        return list;
    }
    //add item to the local db
    public static void addItem(SQLiteDatabase db, Item item) {
        ContentValues values = new ContentValues();
        values.put(ITEM_ID, item.id);
        values.put(ITEM_USERID, item.userId);
        values.put(ITEM_ITEMNAME, item.itemName);
        values.put(ITEM_PRICE, item.price);
        values.put(ITEM_DESC, item.desc);
        values.put(ITEM_LOCATION, item.location);
        values.put(ITEM_TIME, item.lastUpdateDate);
        values.put(ITEM_IMGURL, item.imgUrl);
        values.put(ITEM_DELETE, item.isRemoved);
        Log.d("check","add query");
        db.insert(ITEM_TABLE, ITEM_ID, values);
    }
    //get item from the local with id primary key
    public static Item getItem(SQLiteDatabase db, String itemId) {

        if(itemId!=null) {
            Cursor cursor = db.query(ITEM_TABLE, null, ITEM_ID + "=?", new String[]{itemId}, null, null, null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                int idIndex = cursor.getColumnIndex(ITEM_ID);//
                int userIdIndex = cursor.getColumnIndex(ITEM_USERID);//
                int name = cursor.getColumnIndex(ITEM_ITEMNAME);
                int price = cursor.getColumnIndex(ITEM_PRICE);
                int desc =cursor.getColumnIndex(ITEM_DESC);
                int location =cursor.getColumnIndex(ITEM_LOCATION);
                int time =cursor.getColumnIndex(ITEM_TIME);
                int imgurl =cursor.getColumnIndex(ITEM_IMGURL);
                int delete =cursor.getColumnIndex(ITEM_DELETE);

                Item item = new Item(false);
                item.id = cursor.getString(idIndex);
                item.userId = cursor.getString(userIdIndex);
                item.itemName = cursor.getString(name);
                item.price = cursor.getFloat(price);
                item.desc=cursor.getString(desc);
                item.location = cursor.getString(location);
                item.lastUpdateDate = cursor.getDouble(time);
                item.imgUrl = cursor.getString(imgurl);
                item.isRemoved = cursor.getInt(delete);
                return item;
            }

        }
        return null;

    }
    //search item function, this function return all the items with itemName (pram)
    public static List<Item> searchItem(SQLiteDatabase db, String itemName) {
        List<Item> list = new LinkedList<Item>();
            Cursor cursor = db.query(ITEM_TABLE, null, ITEM_ITEMNAME + "=?", new String[]{itemName}, null, null, null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                int idIndex = cursor.getColumnIndex(ITEM_ID);//
                int userIdIndex = cursor.getColumnIndex(ITEM_USERID);//
                int name = cursor.getColumnIndex(ITEM_ITEMNAME);
                int price = cursor.getColumnIndex(ITEM_PRICE);
                int desc =cursor.getColumnIndex(ITEM_DESC);
                int location =cursor.getColumnIndex(ITEM_LOCATION);
                int time =cursor.getColumnIndex(ITEM_TIME);
                int imgurl =cursor.getColumnIndex(ITEM_IMGURL);
                int delete =cursor.getColumnIndex(ITEM_DELETE);
                do {
                    Item item = new Item(false);
                    item.id = cursor.getString(idIndex);
                    item.userId = cursor.getString(userIdIndex);
                    item.itemName = cursor.getString(name);
                    item.price = cursor.getFloat(price);
                    item.desc=cursor.getString(desc);
                    item.location = cursor.getString(location);
                    item.lastUpdateDate = cursor.getDouble(time);
                    item.imgUrl = cursor.getString(imgurl);
                    item.isRemoved = cursor.getInt(delete);

                    if(item.isRemoved !=1)
                        list.add(item) ;
                } while (cursor.moveToNext());

            }
            Log.d("search","search size:"+list.size());
            return list;
    }
    //this function return all items of one user
    public static List<Item> getItemByUser(SQLiteDatabase db, String UserId) {
        List<Item> list = new LinkedList<Item>();
        Cursor cursor = db.query(ITEM_TABLE, null, ITEM_USERID + "=?", new String[]{UserId}, null, null, null);
        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            int idIndex = cursor.getColumnIndex(ITEM_ID);//
            int userIdIndex = cursor.getColumnIndex(ITEM_USERID);//
            int name = cursor.getColumnIndex(ITEM_ITEMNAME);
            int price = cursor.getColumnIndex(ITEM_PRICE);
            int desc =cursor.getColumnIndex(ITEM_DESC);
            int location =cursor.getColumnIndex(ITEM_LOCATION);
            int time =cursor.getColumnIndex(ITEM_TIME);
            int imgurl =cursor.getColumnIndex(ITEM_IMGURL);
            int delete =cursor.getColumnIndex(ITEM_DELETE);
            do {
                Item item = new Item(false);
                item.id = cursor.getString(idIndex);
                item.userId = cursor.getString(userIdIndex);
                item.itemName = cursor.getString(name);
                item.price = cursor.getFloat(price);
                item.desc=cursor.getString(desc);
                item.location = cursor.getString(location);
                item.lastUpdateDate = cursor.getDouble(time);
                item.imgUrl = cursor.getString(imgurl);
                item.isRemoved = cursor.getInt(delete);
                Log.d("itemdel",""+item.isRemoved);
                if(item.isRemoved !=1)
                    list.add(item) ;
            } while (cursor.moveToNext());

        }
        return list;




    }
    //upgrade the local db function, drop the table
    static public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + ITEM_TABLE );
        onCreate(db);
    }
    //create table function
    static public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + ITEM_TABLE +
                " (" +
                ITEM_ID + " TEXT PRIMARY KEY, " +
                ITEM_ITEMNAME + " TEXT, " +
                ITEM_USERID + " TEXT, " +
                ITEM_IMGURL + " TEXT, "+
                ITEM_DESC+" TEXT, "
                +ITEM_LOCATION+" TEXT, "
                +ITEM_TIME+" DOUBLE, " +
                ITEM_DELETE+" NUMBER, "
                +
                ITEM_PRICE+" DOUBLE);");
    }
    //update item function
    static public void updateitem(SQLiteDatabase db, Item item)
    {
        ContentValues values = new ContentValues();
        values.put(ITEM_ID, item.id);
        values.put(ITEM_USERID, item.userId);
        values.put(ITEM_ITEMNAME, item.itemName);
        values.put(ITEM_PRICE, item.price);
        values.put(ITEM_DESC, item.desc);
        values.put(ITEM_LOCATION, item.location);
        values.put(ITEM_TIME, item.lastUpdateDate);
        values.put(ITEM_IMGURL, item.imgUrl);
        Log.d("gold","item delete 5******:"+item.isRemoved);
        values.put(ITEM_DELETE, item.isRemoved);
        db.update(ITEM_TABLE, values, ITEM_ID+"=?", new String[]{item.id});

    }


}
