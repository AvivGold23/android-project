package com.example.goldav.buybye;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.example.goldav.buybye.model.ModelUser;
import com.example.goldav.buybye.model.User;


/**
 * this is the user details fragment which show the details about antoher user
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDetailsFragment extends Fragment {
    private static final String ARG_PARAM1 = "id";//the user id

    // TODO: Rename and change types of parameters
    private String id;

    private OnFragmentInteractionListener mListener;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment UserDetailsFragment.
     */
    // the factory method of the fragment
    public static UserDetailsFragment newInstance(String param1) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
        setHasOptionsMenu(true);
    }
    //setting the menu
    public void onPrepareOptionsMenu(Menu menu)
    {
        menu.findItem(R.id.edititem).setVisible(false);
        menu.findItem(R.id.mainAdd).setVisible(true);
        menu.findItem(R.id.myItems).setVisible(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=  inflater.inflate(R.layout.fragment_user_details, container, false);
        final EditText name = (EditText) view.findViewById(R.id.UserFullName);
        final EditText phone =(EditText)view.findViewById(R.id.UserFullPhone);
        final EditText Email = (EditText) view.findViewById(R.id.UserFullMail);
        ModelUser.instace.getUser(id, new ModelUser.GetUserCallback() {
            @Override
            public void onComplete(User user) {
                name.setText(user.FirstName+" "+ user.LastName);
                phone.setText(user.Phone);
                Email.setText(user.Email);
            }

            @Override
            public void onCancel() {

            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
