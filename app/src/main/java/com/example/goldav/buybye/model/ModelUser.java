package com.example.goldav.buybye.model;

/**
 * User model class, with singleton dp
 */

public class ModelUser {
    public final static ModelUser instace = new ModelUser();
    private ModelFirebaseUser userFirebase;

    private ModelUser() {
        userFirebase = new ModelFirebaseUser();
    }
    //get user from the server
    public void getUser(String id, final GetUserCallback userCallback) {
        userFirebase.getUser(id, new ModelFirebaseUser.GetUserCallback() {
            @Override
            public void onComplete(User user) {
                userCallback.onComplete(user);
            }

            @Override
            public void onCancel() {
                userCallback.onCancel();
            }
        });
    }

    public interface GetUserCallback {
        void onComplete(User user);

        void onCancel();
    }
    //add user to the server
    public void addUser(User user) {
        userFirebase.addUser(user);

    }
}
