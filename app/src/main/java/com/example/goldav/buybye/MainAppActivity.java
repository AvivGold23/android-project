package com.example.goldav.buybye;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;


import com.example.goldav.buybye.model.ModelItem;


public class MainAppActivity extends Activity implements ProductsListFragment.OnFragmentInteractionListener,
        AddItemFragment.OnFragmentInteractionListener, ItemDetailsFragment.OnFragmentInteractionListener,UserDetailsFragment.OnFragmentInteractionListener{
    FragmentTransaction tran;// transaction manager

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_app);
        ProductsListFragment listFragment = ProductsListFragment.newInstance(null);
        tran = getFragmentManager().beginTransaction();//first we show the all items of the app fragment
        tran.replace(R.id.main_app,listFragment);
        tran.commit();
        getActionBar().setDisplayHomeAsUpEnabled(false);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
            case R.id.mainAdd://add item btn
                AddItemFragment addItem = AddItemFragment.newInstance(null);
                tran = getFragmentManager().beginTransaction() ;
                tran.replace(R.id.main_app,addItem);
                tran.addToBackStack("");
                tran.commit();
                break;
            case R.id.logOut://log uot btn
                MainActivity.mAuth.signOut();//close the connection with fire base
                ModelItem.instace.unRegisterUpdates();//closing the observe
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();//switch to the main activity with the log in screen
                break;
            case R.id.allitems://show all app items
                ProductsListFragment listFragment = ProductsListFragment.newInstance(null);
                tran = getFragmentManager().beginTransaction() ;
                tran.replace(R.id.main_app,listFragment);
                tran.commit();
                getActionBar().setDisplayHomeAsUpEnabled(false);
                break;
            case R.id.myItems://show specifc user items
                ProductsListFragment listFragment1 = ProductsListFragment.newInstance(MainActivity.mAuth.getCurrentUser()
                        .getUid());
                tran = getFragmentManager().beginTransaction() ;
                tran.replace(R.id.main_app,listFragment1);
                tran.commit();
                getActionBar().setDisplayHomeAsUpEnabled(false);
                break;



            default:
                return super.onOptionsItemSelected(item);


        }
        return true;
    }
    //switch to the item details fragment
    @Override
    public void onFragmentInteraction(String id) {
        ItemDetailsFragment dt= ItemDetailsFragment.newInstance(id);
        tran = getFragmentManager().beginTransaction() ;
        tran.replace(R.id.main_app,dt);
        tran.addToBackStack("");
        tran.commit();
        getActionBar().setDisplayHomeAsUpEnabled(true);

    }
    //finish with edit/ add/delete item and switch to the all items fragment
    @Override
    public void finish()
    {
        ProductsListFragment listFragment = ProductsListFragment.newInstance(null);
        tran = getFragmentManager().beginTransaction() ;
        tran.replace(R.id.main_app,listFragment);
        tran.commit();
        getActionBar().setDisplayHomeAsUpEnabled(false);
    }

    //switch the user details screen
    @Override
    public void ShowUser(String id) {
        UserDetailsFragment userDetails = UserDetailsFragment.newInstance(id);
        tran = getFragmentManager().beginTransaction() ;
        tran.replace(R.id.main_app,userDetails);
        tran.addToBackStack("");
        tran.commit();
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
