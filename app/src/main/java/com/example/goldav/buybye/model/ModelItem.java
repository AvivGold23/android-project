package com.example.goldav.buybye.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.URLUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static com.example.goldav.buybye.model.ModelFiles.saveImageToFile;

/**
 * the model of the items, which using the singleton design patterns
 */

public class ModelItem {
    public final static ModelItem instace = new ModelItem();//singleton
    public ModelSqlItem modelSqlItem;
    private ModelFirebaseItem itemFirebase;
    //private ctor for the singleton
    private ModelItem() {
        itemFirebase = new ModelFirebaseItem();
        modelSqlItem = new ModelSqlItem(Buybye.getMyContext());
        synchItemsDbAndregisterItemsUpdates();
    }
    //delete item method, chang item is removed to 1
    public void deleteItem(Item item)
    {
        item.isRemoved=1;
        itemFirebase.addItem(item);
    }
    //get item from the local db method
    public Item getItem(String id)
    {
       return ItemSqlQuerys.getItem(modelSqlItem.getReadableDatabase(),id);
    }
    //get search items from the local db
    public List<Item> searchItems(String text)
    {
        return ItemSqlQuerys.searchItem(modelSqlItem.getReadableDatabase(),text);
    }
    //get list of the user items from the db
    public List<Item> userItems(String userId)
    {
        return ItemSqlQuerys.getItemByUser(modelSqlItem.getReadableDatabase(),userId);
    }
    //add item into the server
    public void addItem(Item item) {
        itemFirebase.addItem(item);
    }
    //get all items from the local db
    public List<Item> getAllItems()
    {
        return ItemSqlQuerys.getAllItems(modelSqlItem.getReadableDatabase());
    }
    //gets updated from the server, with using of the lastupdate date from the shared preferences
    private void synchItemsDbAndregisterItemsUpdates() {
        //1. get local lastUpdateTade
        SharedPreferences pref = Buybye.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        final double lastUpdateDate = pref.getFloat("ItemsLastUpdateDate",0);
        Log.d("TAG","ItemsLastUpdateDate: " + lastUpdateDate);

        itemFirebase.registerItemsUpdates(lastUpdateDate,new ModelFirebaseItem.RegisterItemUpdatesCallback() {
            @Override
            public void onItemUpdate(Item item) {
                //3. update the local db
                if(ModelItem.instace.getItem(item.id)==null)//add case, the item is not  exists the local db
                ItemSqlQuerys.addItem(modelSqlItem.getWritableDatabase(),item);
                else {
                    //update , the item is already exists
                    ItemSqlQuerys.updateitem(modelSqlItem.getWritableDatabase(), item);
                }
                //4. update the lastUpdateTade
                SharedPreferences pref = Buybye.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
                final double lastUpdateDate = pref.getFloat("ItemsLastUpdateDate",0);
                if (lastUpdateDate < item.lastUpdateDate){
                    SharedPreferences.Editor prefEd = Buybye.getMyContext().getSharedPreferences("TAG",
                            Context.MODE_PRIVATE).edit();
                    prefEd.putFloat("ItemsLastUpdateDate", (float) item.lastUpdateDate);
                    prefEd.commit();
                    Log.d("TAG","ItemsLastUpdateDate: " + item.lastUpdateDate);
                }
                //publish the items
                EventBus.getDefault().post(new UpdateItemEvent(item));
            }
        });
    }
    //loguout from updated from the fire base server
    public void unRegisterUpdates() {
        itemFirebase.logout();
    }
    //update event to the event bus
    public class UpdateItemEvent {
        public final Item item;
        public UpdateItemEvent(Item item) {
            this.item = item;
        }
    }
    public interface SaveImageListener {
        void complete(String url);
        void fail();
    }
    //save image into the phone
    public void saveImage(final Bitmap imageBmp, final String name, final SaveImageListener listener) {
        itemFirebase.saveImage(imageBmp, name, new SaveImageListener() {
            @Override
            public void complete(String url) {
                String fileName = URLUtil.guessFileName(url, null, null);
                saveImageToFile(imageBmp,fileName);
                listener.complete(url);
            }

            @Override
            public void fail() {
                listener.fail();
            }
        });


    }
    public interface GetImageListener{
        void onSuccess(Bitmap image);
        void onFail();
    }
    public void getImage(final String url, final GetImageListener listener) {
        //check if image exsist localy
        final String fileName = URLUtil.guessFileName(url, null, null);
        ModelFiles.loadImageFromFileAsynch(fileName, new ModelFiles.LoadImageFromFileAsynch() {
            @Override
            public void onComplete(Bitmap bitmap) {
                if (bitmap != null){
                    Log.d("TAG","getImage from local success " + fileName);
                    listener.onSuccess(bitmap);
                }else {
                    itemFirebase.getImage(url, new GetImageListener() {
                        @Override
                        public void onSuccess(Bitmap image) {
                            String fileName = URLUtil.guessFileName(url, null, null);
                            Log.d("TAG","getImage from FB success " + fileName);
                            saveImageToFile(image,fileName);
                            listener.onSuccess(image);
                        }

                        @Override
                        public void onFail() {
                            Log.d("TAG","getImage from FB fail ");
                            listener.onFail();
                        }
                    });

                }
            }
        });
    }
}
