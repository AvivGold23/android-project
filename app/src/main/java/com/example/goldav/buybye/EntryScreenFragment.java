package com.example.goldav.buybye;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;


import com.example.goldav.buybye.model.Validation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
//this fargemnt is represented the log in screen
public class EntryScreenFragment extends Fragment  {


    private OnFragmentInteractionListener mListener;
    public EntryScreenFragment() {
    }


    public static EntryScreenFragment newInstance() {//factory method
        EntryScreenFragment fragment = new EntryScreenFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    //loading the view and set the listeners
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view =inflater.inflate(R.layout.fragment_entry__screen, container, false);
        final ProgressBar pb = (ProgressBar) view.findViewById(R.id.mainProgressBar6);
        Button LogIn=(Button) view.findViewById(R.id.LogIn);
        LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText Email = (EditText) view.findViewById(R.id.SignInEmail);
                EditText pass = (EditText) view.findViewById(R.id.SignInPass);
                if(!Validation.CheckEditTextsEmpty(new EditText[]{Email,pass})) {//check if one the edit texts is empty
                    FirebaseUser currentUser;
                    //sign in request into the fire base server
                    pb.setVisibility(View.VISIBLE);

                    MainActivity.mAuth.signInWithEmailAndPassword(Email.getText().toString(), pass.getText().toString())
                            .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d("TAG", "createUserWithEmail:success");
                                        MyAlert my = new MyAlert();
                                        my.Message = "work";
                                        my.show(getFragmentManager(), "");
                                        pb.setVisibility(View.INVISIBLE);

                                        mListener.logIn();//switch screen

                                    } else {//failed to sign in and showing alert
                                        MyAlert my = new MyAlert();
                                        my.Message = (task.getException().toString().split(":")[1]);
                                        pb.setVisibility(View.INVISIBLE);

                                        my.show(getFragmentManager(), "");
                                    }
                                }


                            });
                }

            }
        });
        Button SignUp=(Button) view.findViewById(R.id.SignUp);
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.SignUpScreen();
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnFragmentInteractionListener {
        void logIn( );
        void SignUpScreen();
    }

}
