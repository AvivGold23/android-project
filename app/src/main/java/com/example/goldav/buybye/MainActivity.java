package com.example.goldav.buybye;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends Activity implements EntryScreenFragment.OnFragmentInteractionListener,
        SignUpFragment.OnFragmentInteractionListener {
    public static FirebaseAuth mAuth ;//fire base auth member with give us info about
    // the connection with the fire base server
    FragmentTransaction tran;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mAuth=FirebaseAuth.getInstance();
        super.onCreate(savedInstanceState);

        //request permission from the user. (in addition to the manifests)
        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,new String[]{
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
        }
        setContentView(R.layout.activity_main);
        tran =  getFragmentManager().beginTransaction();
        EntryScreenFragment listFragment = EntryScreenFragment.newInstance();
        tran.add(R.id.main_container, listFragment);
        tran.commit();
    }



    @Override
    //the user is log in , and now we load the MainAppActivity with the data of the app.
    public void logIn() {
        Intent intent = new Intent(MainActivity.this,MainAppActivity.class);
        startActivity(intent);

        finish();

    }

    //switch to  the sign up fragment
    @Override
    public void SignUpScreen() {
        SignUpFragment signUpFragment = SignUpFragment.newInstance();
         tran = getFragmentManager().beginTransaction() ;
        tran.replace(R.id.main_container,signUpFragment);
        tran.commit();
    }
    //the user is sign up into the app
    @Override
    public void SignUp() {
        Intent intent = new Intent(MainActivity.this,MainAppActivity.class);
        startActivity(intent);


        finish();
    }


}
