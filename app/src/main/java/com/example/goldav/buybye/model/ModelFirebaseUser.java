package com.example.goldav.buybye.model;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
*user fire base
 *  */

public class ModelFirebaseUser {
    //adding user into the server
    public void addUser(User user) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");
        Map<String, Object> value = new HashMap<>();
        value.put("FirstName", user.FirstName);
        value.put("LastName", user.LastName);
        value.put("id",user.id);
        value.put("Email", user.Email);
        value.put("Phone", user.Phone);
        value.put("lastUpdateDate", ServerValue.TIMESTAMP);
        myRef.child((user.id)).setValue(value);
    }
    interface GetUserCallback {
        void onComplete(User user);

        void onCancel();
    }
    //get user by id from the server
    public void getUser(String id, final GetUserCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");
        myRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                callback.onComplete(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onCancel();
            }
        });
    }

}
