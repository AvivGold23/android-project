package com.example.goldav.buybye.model;

import java.util.Random;

/**
 * The class that represented the items in the buybye app
 */

public class Item {
    public String userId="";
    public String id;
    public String itemName="";//
    public double price;//
    public String desc="";//
    public String location="";
    public double lastUpdateDate;
    public String imgUrl="";
    public int isRemoved;
    //if true we set new id, and check if is  id is alerdy exists
    public Item(boolean bool)
    {
        if(bool) {
            do {
                Random rand = new Random();
                id = String.valueOf(rand.nextInt((int) (Math.pow(2, 31))));
            }while(ModelItem.instace.getItem(id)!=null);
        }
    }

    public Item() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(double lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(int isRemoved) {
        this.isRemoved = isRemoved;
    }
}
