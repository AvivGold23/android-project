package com.example.goldav.buybye.model;


import android.widget.EditText;

/**
 * Created by Aviv Gold on 6/26/2017.
 */

public class Validation {
    //check if one the edit texts is empty
    public static boolean CheckEditTextsEmpty(EditText[] texts)
    {
        boolean check = false;
        for (EditText text :texts) {
            if (text!=null)
            {
                if(text.getText().toString().equals("")) {
                    check = true;
                    text.setError("please fill");
                }
            }
            else {
                check = true;
                text.setError("please fill");
            }

        }
        return check;
    }

}
