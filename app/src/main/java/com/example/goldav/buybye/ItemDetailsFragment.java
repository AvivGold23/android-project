package com.example.goldav.buybye;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.goldav.buybye.model.Item;
import com.example.goldav.buybye.model.ModelItem;

import static android.view.View.GONE;


//te fragemnt of item details
public class ItemDetailsFragment extends Fragment {
    private static final String ARG_PARAM1 = "id";

    private String id;//id of the items
    private OnFragmentInteractionListener mListener;
    ProgressBar progressBar;
    ImageView imageView;//image view of the item

    public ItemDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ItemDetailsFragment.
     */
    public static ItemDetailsFragment newInstance(String param1) {
        ItemDetailsFragment fragment = new ItemDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
        setHasOptionsMenu(true);

    }
    //set the menu item selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //in this case of click on the edit item we replace to edit item fragment
        if (item.getItemId() == R.id.edititem) {
            AddItemFragment s = AddItemFragment.newInstance(id);
            FragmentTransaction tran3 = getFragmentManager().beginTransaction();
            tran3.replace(R.id.main_app, s);
            tran3.addToBackStack("");
            tran3.commit();
        }
        return true;
    }

    //load all item data
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=  inflater.inflate(R.layout.fragment_item_details, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.mainProgressBar2);
        final Item item= ModelItem.instace.getItem(id);
        imageView= (ImageView) view.findViewById(R.id.itemImage2);
        if (item.imgUrl != null && !item.imgUrl.isEmpty() && !item.imgUrl.equals("")) {
            progressBar.setVisibility(View.VISIBLE);
            //image_lable.setVisibility(GONE);
            ModelItem.instace.getImage(item.imgUrl, new ModelItem.GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    imageView.setImageBitmap(image);
                    progressBar.setVisibility(GONE);
                }

                @Override
                public void onFail() {
                    MyAlert my = new MyAlert();
                    my.Message = "failed to load the image";
                    my.show(getFragmentManager(), "");
                    progressBar.setVisibility(View.GONE);

                }
            });

        }
        TextView name = (TextView) view.findViewById(R.id.itemDetailsName);
        name.setText(item.itemName);
        TextView price = (TextView) view.findViewById(R.id.ItemDescPrice);
        price.setText(""+item.price);
        TextView title = (TextView) view.findViewById(R.id.itemTitleDesc);
        title.setText(item.desc);
        TextView location = (TextView) view.findViewById(R.id.itemDescLocation);
        location.setText(item.location);
        Button User = (Button) view.findViewById(R.id.seller);
        User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.ShowUser(item.userId);
            }
        });
        return view;

    }
    //the menu set method
    public void onPrepareOptionsMenu(Menu menu)
    {
        String id5= MainActivity.mAuth.getCurrentUser().getUid();
        String userId =ModelItem.instace.getItem(id).userId;
        if(userId.equals(id5)) {
            menu.findItem(R.id.edititem).setVisible(true);
        }
        menu.findItem(R.id.mainAdd).setVisible(true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void ShowUser(String  id);
    }
}
